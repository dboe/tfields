import pytest
import unittest.mock
import logging


# To add auto-imports for doctests
# import numpy
# @pytest.fixture(autouse=True)
# def add_np(doctest_namespace):
#     doctest_namespace["np"] = numpy


# Patching plotting
@pytest.fixture(scope="session", autouse=True)
def default_session_fixture(request):
    """
    :type request: _pytest.python.SubRequest
    :return:
    """
    log = logging.getLogger()
    try:
        import matplotlib  # noqa:F401 type: ignore[mypy] "matplotlib" is not accessed
    except ImportError as err:
        log.debug("Patching `show` does not work because of ImportError %s", err)
        return

    log.info("Patching `show`")
    patch = unittest.mock.patch("matplotlib.pyplot.show")
    if not request.config.getoption("--plot"):
        patch.start()


def pytest_addoption(parser):
    parser.addoption(
        "--plot", action="store_true", default=False, help="Activate plot tests"
    )
    parser.addoption(
        "--slow", action="store_true", default=False, help="Activate slow tests"
    )


def pytest_configure(config):
    config.addinivalue_line(
        "markers", "plot: mark test as requiring visual assertion by human"
    )
    config.addinivalue_line("markers", "slow: mark test as slow")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--plot") or config.getoption("--slow"):
        return
    skip_plot = pytest.mark.skip(reason="need --plot option to run")
    skip_slow = pytest.mark.skip(reason="need --slow option to run")
    for item in items:
        if "plot" in item.keywords:
            item.add_marker(skip_plot)
        if "slow" in item.keywords:
            item.add_marker(skip_slow)
