============
Installation
============
..
    How to get it and set it up?
    [installation]

To install tfields, you have the following options:

.. tab-set::

    .. tab-item:: PyPi :octicon:`package`

        The preferred method to install tfields is to get the most recent stable release from PyPi:
        
        ..
            [essence-start]

        .. code-block:: shell
       
           pip install tfields
           
        ..
            [essence-end]

        If you don't have `pip`_ installed, this `Python installation guide`_ can guide you through the process.
        
        .. _pip: https://pip.pypa.io
        .. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/

        .. dropdown:: Extras
            :icon: star
            
            Install a special extra:
                :code:`pip install tfields[extra]`

            All extras:
                :code:`pip install tfields[full]`

    .. tab-item:: Source :octicon:`code`

        First you have to retrieve the source code of tfields.
        You have the following options:
        
        .. tab-set::
        
            .. tab-item:: Git :octicon:`git-branch`

                To clone the public repository run

                .. code-block:: shell

                    git clone git://gitlab.mpcdf.mpg.de/dboe/tfields

            .. tab-item:: Tarball :octicon:`gift`

                Either download the tarball `here <https://gitlab.mpcdf.mpg.de/dboe/tfields/tarball/master>`_ or run

                .. code-block:: shell

                    curl -OJL https://gitlab.mpcdf.mpg.de/dboe/tfields/tarball/master

                
        Once you have a copy of the source, navigate inside and install it with:

        .. code-block:: shell

            poetry install


