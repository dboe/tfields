.. div:: sd-text-center sd-text-primary sd-fs-1 sd-font-weight-bolder -sd-text-nowra
   
   tfields

.. card::
   :text-align: center
   :shadow: none
   :class-card: sd-border-0

   .. include:: ../README.rst
      :start-after: [shields-start]
      :end-before: [shields-end]
   
      
.. grid:: 1 1 2 2
    :gutter: 1

    .. grid-item::

        .. grid:: 1 1 1 1
            :gutter: 1

            .. grid-item-card:: Overview
               :link: lib/overview/index
               :link-type: doc

               .. include:: ../README.rst
                  :start-after: [overview-start]
                  :end-before: [overview-end]
      
               .. image:: images/smooth-book-400x400.jpeg

            .. grid-item-card:: Installation
               :link: lib/installation/index
               :link-type: doc

               .. include:: lib/installation/index.rst
                  :start-after: [essence-start]
                  :end-before: [essence-end]

    .. grid-item::

        .. grid:: 1 1 1 1
            :gutter: 1

            .. grid-item-card:: Usage
               :link: lib/usage/index
               :link-type: doc

               .. include:: lib/usage/index.rst
                  :start-after: [essence-start]
                  :end-before: [essence-end]

            .. grid-item-card:: API reference
               :link: lib/reference/_autosummary/tfields
               :link-type: doc
               :text-align: center

               :octicon:`codescan;5em;sd-text-info`

            .. grid-item-card:: API development
               :link: community/index
               :link-type: doc
               
               
               .. code-block:: shell
                  
                  make install
                  make test
                  make publish

.. toctree::
   :hidden:

   about/index
   community/index
   lib/index
