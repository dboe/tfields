..
   [intro-start]

Types of Contributions
----------------------

There are many ways you can contribute to the project, including:

..
   [intro-end]

.. toctree::
   :maxdepth: 2

   bugs_report
   bugs_fix
   features
   docu
   tests
   feedback
 