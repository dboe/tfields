Submit Feedback
~~~~~~~~~~~~~~~

The best way to send feedback is to file an `Issue <https://gitlab.mpcdf.mpg.de/dboe/tfields/-/issues/>`_.

If you are proposing a feature:

* Explain in detail how it would work.
* Keep the scope as narrow as possible, to make it easier to implement.
* Remember that this is a volunteer-driven project, and that contributions
  are welcome
