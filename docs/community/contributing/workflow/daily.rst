Daily Operations
----------------

- Work on the virtual environment (created by `make env`) by activating it::

    .. code-block:: shell

        conda activate ./env

- Work on a branch different from `master/main` for local development::

    .. code-block:: shell

        git checkout -b <name-of-your-bugfix-or-feature>

   Now you can make your changes locally.

- When you're done making changes, check that these pass the linters and tests::

    .. code-block:: shell

        make test
    
- Commit your changes and push your branch to origin::

    .. code-block:: shell

        git add <files to be added to your commit>
        git commit -m "Your detailed description of your changes."
        git push origin name-of-your-bugfix-or-feature

- Submit a `merge request <https://gitlab.mpcdf.mpg.de/dboe/tfields/-/merge_requests/>`_ through the repository website Follow the `Guidelines <#guidelines>` below.
 