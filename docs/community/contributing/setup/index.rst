..
   [intro-start]

Setup and Install
-----------------

Ready to contribute?
Here's how to set up `tfields` for local development.

..
   [intro-end]

1. If you are part of the project team, you are welcome to submit changes through your own development branch in the main repository.
   However, if you are not part of the team, we recommend `forking <https://gitlab.mpcdf.mpg.de/dboe/tfields/-/forks/new/>`_ the project and submitting changes through a pull request from your forked repository.
   This allows us to review and discuss your changes before merging them into the main project.
2. Clone the repository to your local machine::

    .. code-block:: shell

        git clone git@gitlab.mpcdf.mpg.de:dboe/tfields.git

    Change adequately if you forked.

3. Set up your the local development environment::

    .. code-block:: shell
        
        cd tfields/
        make env
    
    `make env` will setup a virtual conda environment for you, install all packages there and install `pre-commit <https://pre-commit.com/>`_.
    
The above will only be necessary once.

You are now ready to go.
Have a look at 
    
    .. code-block:: shell

        make help

to get an overview over the daily operations that are automatized for you.
