import os
import datetime

try:
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib

__source_dir = "docs"
with open(os.path.join(os.path.dirname(__file__), "../pyproject.toml"), "rb") as f:
    __config = tomllib.load(f)

project = __config["tool"]["poetry"]["name"]
author = __config["tool"]["poetry"]["authors"][0]
copyright = str(datetime.date.today().year) + ", " + author
release = __config["tool"]["poetry"]["version"]

extensions = [
    "sphinx.ext.intersphinx",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx_design",
    "sphinx_copybutton",
    "sphinxcontrib.mermaid",
    "sphinx.ext.viewcode",  # Add links to highlighted source code
    "sphinx.ext.doctest",  # Test snippets in the documentation
    "sphinx.ext.autosummary",
    "sphinx_autodoc_typehints",  # Automatically parse type hints
]
autosummary_generate = True  # Turn on sphinx.ext.autosummary

templates_path = ["_templates"]
exclude_patterns = ["_build"]

intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
}

napoleon_google_docstring = True
napoleon_include_special_with_doc = True
napoleon_use_admonition_for_examples = True
napoleon_use_admonition_for_notes = True
napoleon_use_admonition_for_references = False
napoleon_attr_annotations = True

html_show_sourcelink = (
    False  # Remove 'view source code' from top of page (for html, not python)
)
autodoc_inherit_docstrings = True  # If no docstring, inherit from base class
set_type_checking_flag = True  # Enable 'expensive' imports for sphinx_autodoc_typehints
nbsphinx_allow_errors = True  # Continue through Jupyter errors
# autodoc_typehints = "description" # Sphinx-native method. Not as good as sphinx_autodoc_typehints
add_module_names = False  # Remove namespaces from class/method signatures
autoclass_content = (
    "both"  # Both the class’ and the __init__ method’s docstring are concatenated
)
autodoc_class_signature = "separated"
autodoc_default_options = {
    "show-inheritance": True,
}
autodoc_member_order = "bysource"
autodoc_typehints_format = "short"

always_document_param_types = True
typehints_defaults = "comma"

auto_pytabs_min_version = (3, 7)
auto_pytabs_max_version = (3, 11)

autosectionlabel_prefix_document = True

suppress_warnings = ["autosectionlabel.*"]

html_theme = "pydata_sphinx_theme"
html_static_path = ["_static"]
html_css_files = ["style.css"]
html_js_files = ["versioning.js"]
html_favicon = "images/logo-32x32.jpeg"
html_logo = "images/logo-100x100.jpeg"
html_show_sourcelink = False
html_sidebars = {"about/*": []}
html_title = f"{project} Framework"

html_theme_options = {
    "use_edit_page_button": False,
    "show_toc_level": 4,
    "navbar_align": "left",
    "icon_links": [
        {
            "name": "Source",
            "url": __config["tool"]["poetry"]["repository"],
            "icon": f"fa-brands fa-{'github' if 'github' in __config['tool']['poetry']['repository'] else 'gitlab'}",
            "type": "fontawesome",
        },
    ],
    "navbar_end": ["navbar-icon-links"],
    "navbar_persistent": ["search-button", "theme-switcher"],
}

html_context = {
    "navbar_items": {
        "Documentation": "lib/index",
        "Contributing": "community/index",
        "About": "about/index",
    }
}
