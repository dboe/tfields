include tools/Makefile.envs

# OPTIONS
part ?= patch

.PHONY: info
## Show useful debugging information regarding git status, log, and remote config
info:
	@printf $(_INFO) "Package" "$(MODULE) v$(VERSION)" ;
	@echo ""
	@printf $(_INFO) "Git remote" "" ; \
	git remote -v
	@echo ""
	@printf $(_INFO) "Git log" "" ; \
	git log -8 --graph --oneline --decorate --all
	@echo ""
	@printf $(_INFO) "Git status" "" ; \
	git status
	
.PHONY: env-info
## Show useful debugging information regarding the environment
env-info:
	$(CONDA_EXE) config --get
	$(CONDA_EXE) info
	$(CONDA_EXE) list >> $(DEBUG_FILE)

## Lint with flake8 only
lint-flake:
	flake8 tfields tests

## Lint with pylint only
lint-pylint:
	pylint tfields tests
	
## Lint your code
lint: lint-flake lint-pylint
	
## Run your unit tests
test:
	py.test

## Build the coverage
coverage-build:
	py.test --cov=$(MODULE) || true
	coverage html

## Build the coverage html and display it
coverage: coverage-build
	python -m webbrowser htmlcov/index.html

## Remove pre-commit artifacts
clean-pre-commit:
	pre-commit clean

## Clean the documentation artifacts
clean-docs:
	rm -rf docs/_build

.PHONY: clean
## Clean your project from unnecessary artifacts
clean: clean-docs
	rm -f .make.*
	coverage erase
	rm -rf htmlcov
	rm -rf docs/lib/reference/_autosummary/
	rm -rf dist
	rm -rf build
	rm -rf report
	rm -rf .tox
	rm -rf .pytest_cache
	rm -rf *.egg-info
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
	# pre-commit clean

## Export the environment to a requirements.txt file
requirements: pyproject.toml
	poetry export --without-hashes --format=requirements.txt > requirements.txt
	
## Install docs requirements using poetry
install-docs:
ifeq ($(ENVACTIVE),true)
ifeq ($(DOCS_INSTALLED),false)
	${CONDA_RUN}poetry install --with docs --all-extras

endif
else
	@printf $(_WARN) "docs" "CONDA environment is not active. Make sure to activate the environment ($$ make env-activate) or install the requirements manually ($$ poetry install --with docs). The successful build of the following command is your responsibility."
endif

## Serve the docs (constant update)
docs-serve: install-docs
	${CONDA_RUN}sphinx-autobuild docs docs/_build/ -j auto --watch tfields
	
## Build the documentation
docs-build: install-docs
	# "-M html" specifies that we want to build an HTML version of the documentation.
	# "docs" specifies the source directory where the documentation files are located.
	# "docs/_build/" specifies the output directory where the HTML files will be generated.
	# "-a" tells Sphinx to rebuild all files, even if they haven't been modified.
	# "-j auto" instructs Sphinx to use multiple processors to speed up the build process.
	# "-W" turns on warnings during the build process.
	# "--keep-going" tells Sphinx to continue building even if it encounters errors, rather than stopping the build process immediately.
	# "-E" flag to show all warnings. Not yet found an option to remove the *args, **kwargs warnings. Should be handled by napoleon when looking to the code. but no Idea
	${CONDA_RUN}sphinx-build -M html docs docs/_build/ -E -a -j auto --keep-going
	# No need to call apidoc since autosummary is doing that for you even better.

## Build and open the documentation
docs: docs-build
	# open the html slides
	${CONDA_RUN}python -m webbrowser docs/_build/html/index.html

## get up to date with the cookiecutter template 'dough'. First check that no changes are existing.
update:
	# get up to date with the cookiecutter template 'dough'
	# first check that no changes are existing
	@echo $(GITSTATUS)
	@if [ -z $(GITSTATUS) ]; then \
	  	echo "Working directory clean."; \
	else \
		git status; \
	  	echo "Your status is not clean! I can not update!"; \
	    exit 1; \
	fi
	# Starting upgrade
	cookiecutter_project_upgrader

## build the project for publishing
build:
	poetry build

## check the correctness of the build
check-build: build
	twine check dist/*

## Bump the version and publish the package via ci to pypi. Args: part - one of major / minor / patch(default) -> 'make part=minor publish'
publish: lint-flake test check-build requires_version
	# Makefile laggs afterwards with "poetry version --short" properly so we process the output
	$(eval NEW_VERSION := $(shell poetry version $(part) --dry-run -s --no-plugins))
	@echo $(GITSTATUS)
	@if [ -z $(GITSTATUS) ]; then \
	  	echo "Working directory clean."; \
	else \
		git status; \
		printf $(_WARN) "WAIT" "Your Git status is not clean!" ; \
	    if ! $(MAKE) -s confirm ; then \
		    printf $(_ERROR) "KO" "EXIT" ; \
	        exit 1; \
		fi \
	fi
	poetry version $(part)
	git commit -am "release: v$(NEW_VERSION)"
	git tag "v$(NEW_VERSION)"
	git push
	git push --tags

## remove last tag. mostly, because publishing failed
untag:
	# remove last tag. mostly, because publishing failed
	printf $(_WARN) "WAIT" "Do you want to delete the current version tag and push the change?" ; \
	@if [ $(MAKE) -s confirm ]; then \
		git tag -d v$(VERSION) ; \
		git push origin :refs/tags/v$(VERSION) ; \
	else \
		printf $(_ERROR) "ko" "exit" ; \
		exit 1; \
	fi

FORCE: ;

include tools/Makefile.help
