#!/usr/bin/env python

"""Tests for `tfields` package."""

import unittest
import tfields


class TestPackage(unittest.TestCase):
    """Tests for `tfields` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_version_type(self):
        """Assure that version type is str."""

        self.assertIsInstance(tfields.__version__, str)
