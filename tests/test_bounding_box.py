import unittest
import tfields
import numpy as np


class BoundingBox_Test(unittest.TestCase):
    def setUp(self):
        self.mesh = tfields.Mesh3D.grid((5.6, 6.2, 3), (-0.25, 0.25, 4), (-1, 1, 10))

        self.cuts = {"x": [5.7, 6.1], "y": [-0.2, 0, 0.2], "z": [-0.5, 0.5]}

    def test_tree(self):
        # test already in doctests.
        tree = tfields.bounding_box.Node(self.mesh, self.cuts, at_intersection="keep")
        leaves = tree.leaves()
        leaves = tfields.bounding_box.Node.sort_leaves(leaves)
        meshes = [leaf.mesh for leaf in leaves]  # NOQA
        templates = [leaf.template for leaf in leaves]  # NOQA
        special_leaf = tree.find_leaf([5.65, -0.21, 0])  # NOQA


class Searcher_Check(object):
    def setUp(self):
        self.mesh = None
        self.points = None
        self.delta = None
        self.n_sections = None

    @property
    def usual_res(self):
        if not hasattr(self, "_usual_res"):
            self._usual_res = self.mesh.in_faces(self.points, delta=self.delta)
        return self._usual_res

    def test_tree(self):
        tree = tfields.bounding_box.Searcher(self.mesh, n_sections=None)
        box_res = tree.in_faces(self.points, delta=self.delta)
        self.assertTrue(np.array_equal(box_res, self.usual_res))

    def test_tree_n_sections(self):
        tree = tfields.bounding_box.Searcher(self.mesh, n_sections=self.n_sections)
        box_res = tree.in_faces(self.points, delta=self.delta)
        self.assertTrue(np.array_equal(box_res, self.usual_res))


class Searcher_Coarse_Grid_2D_Test(Searcher_Check, unittest.TestCase):
    def setUp(self):
        self.mesh = tfields.Mesh3D.grid((0, 1, 2), (1, 2, 2), (2, 3, 2))
        self.points = tfields.Tensors(
            [[0.5, 1, 2.1], [0.5, 0, 0], [0.5, 2, 2.1], [0.5, 1.5, 2.5]]
        )
        self.delta = 0.0001
        self.n_sections = [5, 5, 5]


if __name__ == "__main__":
    unittest.main()
