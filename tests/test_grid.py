# pylint:disable=missing-function-docstring,missing-module-docstring,missing-class-docstring,invalid-name
import unittest
import itertools
import numpy as np
import tfields
from tfields.lib import grid
from tfields.tensor_grid import TensorGrid
from tests.test_core import TensorFields_Check


class TensorGrid_Test(unittest.TestCase, TensorFields_Check):
    def setUp(self):
        self.bv = [(0, 1, 2j), (2, 4, 3j), (5, 8, 4j)]
        self.iter_order = [0, 1, 2]
        self.res = tfields.Tensors(
            [
                [0.0, 2.0, 5.0],
                [0.0, 2.0, 6.0],
                [0.0, 2.0, 7.0],
                [0.0, 2.0, 8.0],
                [0.0, 3.0, 5.0],
                [0.0, 3.0, 6.0],
                [0.0, 3.0, 7.0],
                [0.0, 3.0, 8.0],
                [0.0, 4.0, 5.0],
                [0.0, 4.0, 6.0],
                [0.0, 4.0, 7.0],
                [0.0, 4.0, 8.0],
                [1.0, 2.0, 5.0],
                [1.0, 2.0, 6.0],
                [1.0, 2.0, 7.0],
                [1.0, 2.0, 8.0],
                [1.0, 3.0, 5.0],
                [1.0, 3.0, 6.0],
                [1.0, 3.0, 7.0],
                [1.0, 3.0, 8.0],
                [1.0, 4.0, 5.0],
                [1.0, 4.0, 6.0],
                [1.0, 4.0, 7.0],
                [1.0, 4.0, 8.0],
            ]
        )
        self._inst = TensorGrid.grid(*self.bv, iter_order=self.iter_order)

    def check_filled(self, tg):
        self.assertTrue(tg.equal(self.res))
        self.assertListEqual(list(tg.base_num_tuples()), self.bv)

    def check_empty(self, tg):
        self.assertTrue(tg.is_empty())

    def test_grid(self):
        self.check_filled(self._inst)

    def test_empty(self):
        tg = TensorGrid.empty(*self.bv, iter_order=self.iter_order)
        self.check_empty(tg)
        tge = tg.explicit()
        self.check_filled(tge)

    def test_coord_sys(self):
        tg = TensorGrid.empty(
            *self.bv, iter_order=self.iter_order, coord_sys=tfields.bases.CYLINDER
        )
        self.assertEqual(tg.coord_sys, tfields.bases.CYLINDER)
        self.assertEqual(tg.base_vectors.coord_sys, tfields.bases.CYLINDER)
        tge = tg.explicit()
        self.assertEqual(tge.coord_sys, tfields.bases.CYLINDER)

        # transformation does not change the base_vector coord_sys
        tg.transform(tfields.bases.CARTESIAN)
        self.assertEqual(tg.coord_sys, tfields.bases.CARTESIAN)
        self.assertEqual(tg.base_vectors.coord_sys, tfields.bases.CYLINDER)
        tge = tg.explicit()
        self.assertEqual(tge.coord_sys, tfields.bases.CARTESIAN)

    def test_getitem(self):
        tg = TensorGrid.empty(*self.bv, iter_order=self.iter_order)
        self.check_filled(tg[:])
        tge = tg.explicit()
        self.check_filled(tge[:])
        tf = tge[:-2]
        self.assertTrue(tf.equal(self.res[:-2]))
        self.assertIsInstance(tf, tfields.TensorFields)


class TensorGrid_Test_Permutation1(TensorGrid_Test):
    def setUp(self):
        self.bv = [(0, 1, 2j), (2, 4, 3j), (5, 8, 4j)]
        self.iter_order = [2, 0, 1]
        self.res = tfields.Tensors(
            [
                [0.0, 2.0, 5.0],
                [0.0, 3.0, 5.0],
                [0.0, 4.0, 5.0],
                [1.0, 2.0, 5.0],
                [1.0, 3.0, 5.0],
                [1.0, 4.0, 5.0],
                [0.0, 2.0, 6.0],
                [0.0, 3.0, 6.0],
                [0.0, 4.0, 6.0],
                [1.0, 2.0, 6.0],
                [1.0, 3.0, 6.0],
                [1.0, 4.0, 6.0],
                [0.0, 2.0, 7.0],
                [0.0, 3.0, 7.0],
                [0.0, 4.0, 7.0],
                [1.0, 2.0, 7.0],
                [1.0, 3.0, 7.0],
                [1.0, 4.0, 7.0],
                [0.0, 2.0, 8.0],
                [0.0, 3.0, 8.0],
                [0.0, 4.0, 8.0],
                [1.0, 2.0, 8.0],
                [1.0, 3.0, 8.0],
                [1.0, 4.0, 8.0],
            ]
        )
        self._inst = TensorGrid.grid(*self.bv, iter_order=self.iter_order)


class TensorGrid_Test_IO_Change(unittest.TestCase):
    def test_copy_constructor(self):
        tgt = TensorGrid_Test()
        tgt.setUp()
        tg_orig = tgt._inst

        tg_copy = tfields.TensorGrid(tg_orig)
        tgt.check_filled(tg_copy)

        tg_copy_new_num = tfields.TensorGrid(tg_orig, num=[1, 1, 1])
        self.assertListEqual(list(tg_copy_new_num.num), [1, 1, 1])
        self.assertEqual(len(tg_copy_new_num), 1)

    def test_change_iter_order(self):
        t1 = TensorGrid_Test()
        t1.setUp()
        t2 = TensorGrid_Test_Permutation1()
        t2.setUp()

        # pylint:disable=protected-access
        tg1 = t1._inst
        tg1.fields.append(tfields.Tensors(tg1))
        tg1_original = tg1.copy()
        # pylint:disable=protected-access
        tg2 = t2._inst
        tg2.fields.append(tfields.Tensors(tg2))

        tg1.change_iter_order(t2.iter_order)
        self.assertTrue(tg1.equal(tg2))
        self.assertFalse(tg1.equal(tg1_original))
        tg1.change_iter_order(t1.iter_order)
        self.assertTrue(tg1.equal(tg1_original))

    def test_lib_forward_backward(self):
        bv_lengths = [2, 3, 4]
        forw = grid.change_iter_order(bv_lengths, [0, 1, 2], [2, 0, 1])
        back = grid.change_iter_order(bv_lengths, [2, 0, 1], [0, 1, 2])
        self.assertTrue((np.array(forw)[back] == np.arange(np.prod(bv_lengths))).all())

    def test_lib_multi(self):
        bv_lengths = [2, 3, 4]
        # pylint:disable=unused-variable
        for iter_order in itertools.permutations([0, 1, 2]):
            # pylint:disable=unused-variable
            for new_iter_order in itertools.permutations([0, 1, 2]):
                forw = grid.change_iter_order(bv_lengths, [0, 1, 2], [2, 0, 1])
                back = grid.change_iter_order(bv_lengths, [2, 0, 1], [0, 1, 2])
                self.assertTrue(
                    (np.array(forw)[back] == np.arange(np.prod(bv_lengths))).all()
                )
