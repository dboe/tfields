import numpy as np
import unittest

import tfields


class Triangles3D_Test(unittest.TestCase):
    def setUp(self):
        self.mesh = tfields.Mesh3D([[1, 0, 0], [0, 1, 0], [0, 0, 0]], faces=[[0, 1, 2]])
        self.inst = self.mesh.triangles()
        self.point = tfields.Points3D([[0.2, 0.2, 0]])
        self.expected_mask = np.array([True], dtype=bool)
        self.points = tfields.Points3D([[0.2, 0.2, 0], [0.2, 0.2, 0]])
        self.expected_face_indices = np.array([0, 0], dtype=int)
        self.delta = 0.0

    def test__in_triangles(self):
        self.assertTrue(
            np.array_equal(
                self.inst._in_triangles(self.point, delta=self.delta),
                self.expected_mask,
            )
        )

    def test_in_triangles(self):
        indices = self.inst.in_triangles(self.points, delta=self.delta)
        self.assertTrue(np.array_equal(indices, self.expected_face_indices))

    def test_bounding_box_and_mesh_in_faces(self):
        tree = tfields.bounding_box.Searcher(self.mesh)
        box_res = tree.in_faces(self.points, delta=self.delta)
        usual_res = self.mesh.in_faces(self.points, delta=self.delta)
        self.assertTrue(np.array_equal(box_res, usual_res))


class Triangles3D_Test_Raises(Triangles3D_Test):
    def test_raises(self):
        # Raises:
        # Wrong format of point will raise a ValueError
        with self.assertRaises(ValueError) as ctx:
            self.inst._in_triangles(self.points)
        self.assertEqual(
            "point must be castable to shape 3 but is of shape (2, 3)",
            str(ctx.exception),
        )

    def test__in_triangles_not_invertible(self):
        # If you define triangles that have colinear side vectors or in general lead to
        # not invertable matrices the you will always get False
        mesh = tfields.Mesh3D(
            [[0, 0, 0], [2, 0, 0], [4, 0, 0], [0, 1, 0]], faces=[[0, 1, 2], [0, 1, 3]]
        )

        with self.assertWarns(UserWarning):
            mask = mesh.triangles()._in_triangles(np.array([0.2, 0.2, 0]), delta=0.3)
        self.assertTrue(np.array_equal(mask, np.array([False, True], dtype=bool)))


class Triangles3D_Test_TwoFaces(Triangles3D_Test):
    def setUp(self):
        self.mesh = tfields.Mesh3D(
            [[1, 0, 0], [0, 1, 0], [0, 0, 0], [4, 0, 0], [4, 4, 0], [8, 0, 0]],
            faces=[[0, 1, 2], [3, 4, 5]],
        )
        self.inst = self.mesh.triangles()
        self.point = np.array([0.2, 0.2, 0])
        self.expected_mask = np.array([True, False], dtype=bool)
        self.points = tfields.Points3D([self.point])
        self.expected_face_indices = np.array([0], dtype=int)
        self.delta = 0.0


class Triangles3D_Test_TwoFacesOther(Triangles3D_Test_TwoFaces):
    def setUp(self):
        super().setUp()
        self.point = np.array([5, 2, 0])
        self.expected_mask = np.array([False, True], dtype=bool)
        self.points = tfields.Points3D([self.point])
        self.expected_face_indices = np.array([1], dtype=int)


class Triangles3D_TestDeltaOut(Triangles3D_Test):
    def setUp(self):
        self.mesh = tfields.Mesh3D(
            [[1, 0, 0], [0, 1, 0], [0, 0, 0], [4, 0, 0], [4, 4, 0], [8, 0, 0]],
            faces=[[0, 1, 2], [3, 4, 5]],
        )
        self.inst = self.mesh.triangles()
        self.point = np.array([0.2, 0.2, 9000])
        self.expected_mask = np.array([False, False], dtype=bool)
        self.points = tfields.Points3D([self.point])
        self.expected_face_indices = np.array([-1], dtype=int)
        self.delta = 0.0


class Triangles3D_TestDeltaIn(Triangles3D_TestDeltaOut):
    def setUp(self):
        super().setUp()
        self.point = np.array([0.2, 0.2, 0.1])
        self.expected_mask = np.array([True, False], dtype=bool)
        self.points = tfields.Points3D([self.point])
        self.expected_face_indices = np.array([0], dtype=int)
        self.delta = 0.2

    def test_bounding_box_and_mesh_in_faces(self):
        # TODO-0(@dboe): this is a known issue. You have to allow delta for boxes in bounding box.
        # NYI
        pass


class Triangles3D_TestDeltaNone(Triangles3D_TestDeltaOut):
    # if you set delta to None, the minimal distance point(s) are accepted automatically
    def setUp(self):
        super().setUp()
        self.point = np.array([0.2, 0.2, 0.1])
        self.expected_mask = np.array([True, False], dtype=bool)
        self.points = tfields.Points3D([self.point])
        self.expected_face_indices = np.array([0], dtype=int)
        self.delta = None

    def test_bounding_box_and_mesh_in_faces(self):
        # TODO-0(@dboe): this is a known issue. You have to allow delta for boxes in bounding box.
        # NYI
        pass
