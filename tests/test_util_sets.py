import unittest
import numpy as np
import tfields


class RemapTest(unittest.TestCase):
    def setUp(self):
        self.method = tfields.lib.sets.remap

    def test_remap_1d(self):
        array = np.array([1, 2, 2, 1])
        keys = np.array([1, 2])
        values = np.array([0, 10])
        solution = np.array([0, 10, 10, 0])

        result = self.method(array, keys, values)
        comparison = result == solution
        self.assertTrue(comparison.all())
        self.assertIsNot(array, result)

    def test_remap_inplace(self):
        array = np.array([1, 2, 3, 1])
        keys = np.array([1, 2])
        values = np.array([0, 10])
        solution = np.array([0, 10, 3, 0])

        # inplace
        result = self.method(array, keys, values, inplace=True)
        comparison = result == solution
        self.assertTrue(comparison.all())
        self.assertIs(array, result)

    def test_remap(self):
        array = np.array([1, 3, 2, 1]).reshape(2, 2)
        keys = np.array([1, 2])
        values = np.array([0, 10])
        solution = np.array([[0, 3], [10, 0]])

        result = self.method(array, keys, values)
        comparison = result == solution
        self.assertTrue(comparison.all())

    def test_remap_large(self):
        shape = (20, 200)
        num = np.prod(shape)
        array = np.arange(num).reshape(shape)
        keys = np.arange(num)
        values = np.flip(np.arange(num))
        solution = np.flip(np.arange(num)).reshape(shape)

        result = self.method(array, keys, values)
        comparison = result == solution
        self.assertTrue(comparison.all())

    def test_remap_incomplete(self):
        shape = (4, 4)
        num = np.prod(shape)
        array = np.arange(num).reshape(shape)
        keys = np.arange(num)
        values = np.arange(num - 1, -1, -1)
        keys = keys[1:-1]
        values = values[1:-1]
        solution = np.flip(np.arange(num)).reshape(shape)
        solution[0, 0] = 0  # skipped first value in remap keys
        solution[-1, -1] = num - 1  # skipped first value in remap keys

        result = self.method(array, keys, values)
        comparison = result == solution
        self.assertTrue(comparison.all())
