from tempfile import NamedTemporaryFile
import tfields
import numpy as np
import unittest
import sympy
import os
import sys
from tests.test_core import TensorMaps_Check

THIS_DIR = os.path.dirname(
    os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__)))
)
sys.path.append(os.path.normpath(os.path.join(THIS_DIR)))


class Mesh3D_Check(TensorMaps_Check):
    def test_cut_split(self):
        x, y, z = sympy.symbols("x y z")
        self._inst.cut(x + 1.0 / 100 * y > 0, at_intersection="split")

    def test_triangle(self):
        tri = self._inst.triangles()
        mesh = tri.mesh()
        tri_2 = mesh.triangles()  # mesh will not nec. be equal to self._inst
        # that is ok because self._inst could have stale indices etc..
        self.assertTrue(tri.equal(tri_2))

    def test_save_obj(self):
        out_file = NamedTemporaryFile(suffix=".obj")
        self._inst.save(out_file.name)
        _ = out_file.seek(0)  # this is only necessary in the test
        load_inst = type(self._inst).load(out_file.name)
        self.demand_equal(load_inst)


class Square_Sparse_Test(Mesh3D_Check, unittest.TestCase):
    def setUp(self):
        self._inst = tfields.Mesh3D.plane((0, 1, 2j), (0, 1, 2j), (0, 0, 1j))

    def test_cut_keep(self):
        x = sympy.symbols("x")
        cut_inst = self._inst.cut(x < 0.5, at_intersection="keep")
        self.assertTrue(self._inst.equal(cut_inst))
        cut_inst = self._inst.cut(x > 0.5, at_intersection="keep")
        self.assertTrue(self._inst.equal(cut_inst))


class Square_Dense_Test(Mesh3D_Check, unittest.TestCase):
    def setUp(self):
        self._inst = tfields.Mesh3D.plane((0, 1, 5j), (0, 1, 5j), (0, 0, 1j))

    def test_cut_keep(self):
        x = sympy.symbols("x")
        cut_inst = self._inst.cut(x < 0.9, at_intersection="keep")
        self.assertTrue(self._inst.equal(cut_inst))
        self.assertEqual(len(cut_inst), 25)
        cut_inst = self._inst.cut(x > 0.9, at_intersection="keep")
        self.assertEqual(len(cut_inst), 10)


class Box_Sparse_Test(Mesh3D_Check, unittest.TestCase):
    def setUp(self):
        self._inst = tfields.Mesh3D.grid((0, 1, 2), (1, 2, 2), (2, 3, 2))

    def test_cut_split(self):
        from sympy.abc import x

        cuts = self._inst.cut(x < 0.2, at_intersection="split")
        print(cuts)
        self.assertTrue(
            cuts.equal(
                [
                    [0.0, 1.0, 2.0],
                    [0.0, 1.0, 3.0],
                    [0.0, 2.0, 2.0],
                    [0.0, 2.0, 3.0],
                    [0.2, 1.0, 2.8],
                    [0.2, 1.0, 2.0],
                    [0.2, 1.0, 3.0],
                    [0.2, 1.8, 2.0],
                    [0.2, 2.0, 2.0],
                    [0.2, 2.0, 2.8],
                    [0.2, 2.0, 3.0],
                    [0.2, 1.8, 3.0],
                ],
                rtol=1e-12,
            )
        )
        self.assertTrue(
            cuts.maps[3].equal(
                [
                    [0, 1, 2],
                    [1, 2, 3],
                    [0, 1, 4],
                    [0, 4, 5],
                    [1, 4, 6],
                    [0, 2, 7],
                    [0, 7, 5],
                    [2, 7, 8],
                    [2, 3, 9],
                    [2, 9, 8],
                    [3, 9, 10],
                    [1, 3, 11],
                    [1, 11, 6],
                    [3, 11, 10],
                ]
            )
        )


class Sphere_Test(Mesh3D_Check, unittest.TestCase):
    def setUp(self):
        basis_points = 4
        self._inst = tfields.Mesh3D.grid(
            (1, 1, 1),
            (-np.pi, np.pi, basis_points),
            (-np.pi / 2, np.pi / 2, basis_points),
            coord_sys="spherical",
        )
        self._inst.transform("cartesian")
        self._inst[:, 1] += 2
        clean = self._inst.cleaned()
        # self.demand_equal(clean)
        self._inst = clean


class IO_Stl_test(unittest.TestCase):  # no Mesh3D_Check for speed
    def setUp(self):
        self._inst = tfields.Mesh3D.plane((0, 1, 2j), (0, 1, 2j), (0, 0, 1j))

    def test_load_save_stl(self):
        out_file = NamedTemporaryFile(suffix=".stl")
        self._inst.save(out_file.name)
        _ = out_file.seek(0)

        load_inst = type(self._inst).load(out_file.name)
        self.demand_equal(load_inst)

    def demand_equal(self, other):
        self.assertTrue(self._inst.equal(other))


if __name__ == "__main__":
    unittest.main()
