[tool.poetry]
name = "tfields"
version = "0.5.0"
description = "Tensors, tensor fields, graphs, mesh manipulation, CAD and more on the basis of numpy.ndarrays. All objects keep track of their coordinate system. Symbolic math operations work for object manipulation."
authors = ["Daniel Böckenhoff <dboe@ipp.mpg.de>"]
license = "MIT License"
readme = "README.rst"
repository = "https://gitlab.mpcdf.mpg.de/dboe/tfields"
documentation = "https://dboe.pages.mpcdf.de/tfields"
classifiers = [
    # find the full list of possible classifiers at https://pypi.org/classifiers/
    "Development Status :: 3 - Alpha",
    "License :: OSI Approved :: MIT License",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
]
keywords = ["tensors", " tensor-fields", " graphs", " mesh", " numpy", " math"]

[tool.poetry.dependencies]
python = ">=3.8,<4.0"
pathlib = { version = "*", python = "<3.10" }
numpy = ">=1.20.0"
sympy = "<=1.6.2"
scipy = ">=1.9.2"  # symfit will want to have 1.1.0 but e.g. tensorflow needs 1.4.1 - also https://github.com/tBuLi/symfit/issues/277 - python3.10/11 requires somewhere before 1.9.2
rna = ">=0.10.0"
sortedcontainers = "*"
numpy-stl = { version = "*", optional = true}

[tool.poetry.group.dev.dependencies]
poetry-bumpversion = "*"
ensureconda = "*"  # is part of conda-lock either way
conda-lock = "*"
tomli = { version = ">=1.1.0", python = "<3.11" }
pytest = "*"
pytest-cov = "*"
pytest-shutil = "*"
pytest-virtualenv = "*"
pytest-fixture-config = "*"
pytest-xdist = "*"
coverage = { version = "*", extras = ["toml"]}
pylint = "*"
flake8 = "*"
ipdb = "*"
mypy = "*"
twine  = "*"  # for publishing
pre-commit = "*"  # https://pre-commit.com/ for hook managment
pre-commit-hooks = "*"
sphinx = "*"  # for documentation
cookiecutter_project_upgrader = "*"

[tool.poetry.group.docs]
optional = true

[tool.poetry.group.docs.dependencies]
black = ">=22.12.0"
httpx = ">=0.23.2"
uvicorn = ">=0.20.0"
sphinx-autobuild = ">=2021.3.14"
"sphinx-autopackagesummary" = ">=1.3"
sphinx-design = ">=0.3.0"
sphinx = ">=5.3.0"
sphinx-toolbox = ">=3.2.0"
sphinx-copybutton = ">=0.5.1"
sphinxcontrib-mermaid = ">=0.7.1"
pydata-sphinx-theme = ">=0.12.0"
sphinx-autodoc-typehints = ">=1.22"

[tool.poetry.extras]
io = ["numpy-stl"]

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"

[tool.coverage.report]
show_missing = true
exclude_lines = [
    "pragma: no cover",
    "if False",
]
[tool.coverage.run]
omit = [
	"tfields/plotting/*.py"
]

[tool.poetry_bumpversion.file."tfields/__init__.py"]
search = '__version__ = "{current_version}"'
replace = '__version__ = "{new_version}"'

[tool.poetry_bumpversion.file."docs/cookiecutter_input.json"]
search = '"package_version": "{current_version}"'
replace = '"package_version": "{new_version}"'

[tool.pytest.ini_options]
addopts = """
--doctest-modules
--ignore-glob=*/bin/activate
--ignore=performance
"""
junit_family = "xunit2"

[tool.tox]
legacy_tox_ini = """
    [tox]
    isolated_build = true
    skip_missing_interpreters = true
    recreate = true
    min_version = 4.0
    env_list =
        py35
        py36
        py37
        py38
        py39
        py310
        type

    [testenv]
    deps = pytest
    commands = pytest --import-mode importlib

    [testenv:type]
    deps = mypy
    commands = mypy src
"""
