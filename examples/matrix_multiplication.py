import tfields
import numpy as np

t = tfields.TensorFields([[0.3, 0.3]], [[1, 2]])

# Rotation matrix by 3/4 pi
r = tfields.Tensors([[[-1., +1.],
                      [-1., -1.]]]) / np.sqrt(2)

t_rotated = np.dot(t, r)
print(t_rotated, t_rotated.fields[0])
