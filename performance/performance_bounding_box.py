# import tfields
# import numpy as np
# import cPickle as pickle
#
# with tfields.lib.log.timeit("Building Grid"):
#     mesh = tfields.Mesh3D.grid((0, 1, 10), (2, 1, 10), (2, 3, 20))
#     # mesh = tfields.Mesh3D.grid((0, 1, 50), (2, 1, 20), (2, 3, 50))
# print(mesh.nfaces())
#
# with open(tfields.lib.in_out.resolve('~/tmp/node.pickle'), 'r') as f:
#     tree = pickle.load(f)
#
# # with tfields.lib.log.timeit("SEARCHING TREE OFFSET"):
# #     # tree = tfields.bounding_box.Searcher(mesh, n_sections=[2, 1, 2])
# tree = tfields.bounding_box.Searcher(mesh)
#
# points = tfields.Tensors([[0.5, 1, 2.1],
#                           [0.5, 0, 0],
#                           [0.5, 2, 2]])
# with tfields.lib.log.timeit("SEARCHING TREE METHOD"):
#     box_res = tree.in_faces(points, delta=0.0001)
# with tfields.lib.log.timeit("STANDARD METHOD"):
#     usual_res = mesh.in_faces(points, delta=0.0001)
# assert np.array_equal(box_res, usual_res)
#
# with open(tfields.lib.in_out.resolve('~/tmp/node.pickle'), 'w') as f:
#     pickle.dump(tree, f)
