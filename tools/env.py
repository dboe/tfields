import shutil
from .process import run_shell_command
import tempfile
import os
from ensureconda.resolve import platform_subdir


TMPDIR = tempfile.gettempdir()
BOOTSTRAPDIR = os.path.join(TMPDIR, "bootstrap")
PROJECT_DIR = os.path.abspath(os.path.join(__file__, "../.."))
ENVIRONMENT_YAML = "environment.yml"
ENVIRONMENT_YAML_PATH = os.path.join(PROJECT_DIR, ENVIRONMENT_YAML)
POETRY_LOCK = "poetry.lock"
CONDA_LOCK = f"conda-{platform_subdir()}.lock"


def install(conda_run=""):
    run_shell_command(f"{conda_run}poetry install")
    run_shell_command(f"{conda_run}pre-commit install")


def update(conda_run=""):
    # Update Poetry packages and re-generate poetry.lock
    run_shell_command(f"{conda_run}poetry update")
    run_shell_command(f"{conda_run}pre-commit install")


def create(name="env"):
    """
    This create routine was initially influenced by
    https://stackoverflow.com/questions/70851048/does-it-make-sense-to-use-conda-poetry
    """
    env_dir = os.path.join(PROJECT_DIR, name)

    requirements_here = ["poetry", "mamba", "conda-lock"]
    requirements_not_satisfied = any(
        [shutil.which(name) is None for name in requirements_here]
    )

    run_bs = ""
    if requirements_not_satisfied:
        # Use (and if necessary create) a bootstrap env
        if not os.path.exists(BOOTSTRAPDIR):
            # use python 3.10 for bootstraping
            requirements_here.append("python==3.10")
            run_shell_command(
                f"conda create --yes -p {BOOTSTRAPDIR} -c conda-forge "
                + " ".join(requirements_here)
            )
        run_bs = f"conda run -p {BOOTSTRAPDIR} "

    # Create conda lock file(s) from environment.yml or update it if environment.yml has changed.
    run_shell_command(
        f"{run_bs}conda-lock --check-input-hash -k explicit --conda mamba"
    )

    run_env = f"conda run -p {env_dir} "
    if os.path.exists(env_dir):
        # update
        run_shell_command(f"{run_bs}mamba update --file {CONDA_LOCK} -p {env_dir}")
        update(conda_run=run_env)
    else:
        # create
        run_shell_command(f"{run_bs}mamba create --file {CONDA_LOCK} -p {env_dir}")
        install(conda_run=run_env)

    # Add conda and poetry lock files
    run_shell_command("git add *.lock")
    print(
        ">>> done. Generated lock files. You might want to run >>> git commit -m 'build(conda): lock-files'"
    )
